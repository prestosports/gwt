# GWT Project

This repo hosts a forked versrion of the official [GWT repo](https://github.com/gwtproject/gwt). In particular, the 2.4.0 tag version.

The motivation to do so is that the before mentioned version is the one used by the CMS and is no longer officially supported. There were a few occasions where bugs came up, specially when a new browser version was released, and those has to be dealt with in a custom way.

# Build
In order to build the libraries needed by the CMS, in addition to clone this repo you have to: 
  - Clone the **tools** repo located [here](https://github.com/gwtproject/tools)
  - The tools repo should be at the same level that this one
  - To create the SDK distribution files run:
    ```sh
    $ ant clean dist-dev
    ```
   - If the build was successful, the libraries should be located at 
      ```../build/lib```

# Troubleshooting

Make sure you're JAVA_HOME environment variable points to a JDK 1.6 version
  